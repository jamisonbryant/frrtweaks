# vCard Creator

*This file is a **userscript**, which is a script that is used to customize the
behavior or appearance of a website or websites. It is installed with special
software that runs in your browser. For more information on userscripts, see 
https://tampermonkey.net or http://userscripts-mirror.org.*

## Description
Collects contact information from a member page and assembles a vCard file 
which can then be imported into Outlook, Apple Mail, Gmail, or any compatible 
contact management program.

## Installation
Install with [Tampermonkey](https://tampermonkey.net). No special installation
instructions.

## Usage
1. Navigate to a member's page (URL looks like \*.frr.io/member/XYZ)
2. Click the button labeled "Download vCard" 
3. Import the downlaoded file into your contact management program
