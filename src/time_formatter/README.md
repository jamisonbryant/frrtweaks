# Time Formatter

*This file is a **userscript**, which is a script that is used to customize the
behavior or appearance of a website or websites. It is installed with special
software that runs in your browser. For more information on userscripts, see 
https://tampermonkey.net or http://userscripts-mirror.org.*

## Description
Converts various instances of 12-hr time to 24-hr time.

## Installation
Install with [Tampermonkey](https://tampermonkey.net). No special installation
instructions.

## Usage
No special usage instructions.
