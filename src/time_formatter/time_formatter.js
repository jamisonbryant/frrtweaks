// ==UserScript==
// @name         Fire Rescue Reports (FRR) Time Formatter
// @namespace    http://ferrorobotics.com/opensource/frr/tweaks
// @version      0.1
// @description  Converts various instances of 12-hr time to 24-hr time
// @author       Jamison Bryant, Arbutus Volunteer Fire Deparment, Baltimore Cty., MD, USA
// @include      https://*.frr.io/*
// @grant        none
// @require      http://code.jquery.com/jquery-3.2.1.min.js
// @require	     https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js
// ==/UserScript==

(function() {
    'use strict';
    console.log('FRR Time Formatter loaded');

    $(function() {
        // Constants
        const new_format_full = 'YYYY-MM-DD HH:mm';
        const new_format_date = 'YYYY-MM-DD';
        const new_format_time = 'HH:mm';

        // Reusable variables
        var old_format;
        var old_date;
        var new_date;

        // Date/time (top right near send email/logout buttons)
        old_format = 'M/D/YYYY h:mm a';
        old_date = moment($('#date_time').text(), old_format);
        new_date = old_date.format(new_format_full);
        $('#date_time').text(new_date);
    });
})();